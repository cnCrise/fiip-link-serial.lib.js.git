# fiip-link-serial

fiip link layer of serial.

## Installation

```bash
npm install fiip-link-serial --save
```

## Usage

```js
const linkSerial = require('fiip-link-serial');
linkSerial.startClient('/dev/ttyS0', 9600);
```

## License

MIT

## Reference

[git](https://gitee.com/cnCrise/fiip-link-serial.lib.js.git)
