# fiip-link-serial

基于 串口 的 fiip 链路层.

## 安装

```bash
npm install fiip-link-serial --save
```

## 使用

```js
const linkSerial = require('fiip-link-serial');
linkSerial.startClient('/dev/ttyS0', 9600);
```

## 许可证

MIT

## 相关链接

[git](https://gitee.com/cnCrise/fiip-link-serial.lib.js.git)
