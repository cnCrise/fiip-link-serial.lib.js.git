'use strict';
const startClient = require('./client');
const startServer = require('./server');

module.exports = { startServer, startClient };
